#!/bin/bash

downloads_dir=/usr/share/dqsegdb_web/downloads
sanity_count=4900   # 'ls -1 $downloads_dir | wc --lines' returns 4909 on 2024.07.23
target_dir=/backup/segdb/reference/install_support/segments-web
time_log_file=/backup/segdb/segments/monitor/backup_dqsegdb_web_downloads.txt
exit_code=0
backup_date=$(date +%Y-%m-%d)
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")


echo "### INFO ### Starting backup of /usr/share/dqsegdb_web/downloads/ - $(date)"
echo "         ### Output dir = $target_dir"

# sanity check to make sure the downloads dir hasn't been wiped out, in which case a backup of the emptied dir would overwrite the last backup of a good dir
dir_file_count=$(ls -1 $downloads_dir | wc --lines)
if [ "$dir_file_count" -lt "$sanity_count" ]
then
  echo "### ERROR ### Too few files in downloads dir ($downloads_dir) - found $dir_file_count, but expected at least $sanity_count."
  echo "          ### Downloads dir might have been damaged.  Not creating a new backup, to avoid writing over the last good backup (in $target_dir)."
  exit 1
fi

tar czf ${target_dir}/downloads.tgz  -C  /usr/share/dqsegdb_web/  ./downloads
exit_code=$?
if [ "$exit_code" -ne 0 ]; then echo "### ERROR ### Backup of $downloads_dir encountered a problem (exit code = $exit_code).  This should be investigated."; fi

if [ $(date +%d) -eq 1 ]; then cp -p  ${target_dir}/downloads.tgz  ${target_dir}/downloads_$(date +%Y.%m.%d).tgz; fi   # on the 1st of every month, save a permanent copy of the tarball

echo "### INFO ### Writing end time to $time_log_file"
end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))
echo "$start_date  $start_time - $end_time = $duration_sec sec" >> $time_log_file
#echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> $time_log_file
echo "### INFO ### Finished backup of /usr/share/dqsegdb_web/downloads/ - $(date)"

exit $exit_code
