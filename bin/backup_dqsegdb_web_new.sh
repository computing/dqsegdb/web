#!/bin/bash
### Usage: change the variables in the first block of code, save the script, then run it.
### Note: Unlike the regular 'backup_dqsegdb_mysqldatabase.sh' (which is run by a cron job on segments.ligo.org),
###         this version will not save any messages about creating the backup, not to 'dqsegdb_regression_tests' on segments-backup, nor any other DB or machine;
###         also, this version will not change any permissions on output directories - the user must do that before running the script.
### To do:
###   * check that both the user running the script and user 'mysql' can write to the raw output dir; possible problem if run by user other than root, b/c of the 'sudo -u mysql' part
###   * check that the DB that is supposed to be backed up exists
###   * handle the case of the output tarball already existing by creating a different output file?
###   * tarball the dir that the DB files are in, rather than just the DB files?
###   * delete the backed-up files after tarballing them? maybe as an option?
###   * delete the raw output dir, if it was created?  what about if several layers of dirs had been created?

# Setup variables
start_date=$(date +%Y.%m.%d)
start_time=$(date +%H:%M:%S)
start_sec=$(date -u +"%s")
time_log_file=/backup/segdb/segments/monitor/backup_dqsegdb_web_new.txt
# This is the name of the DB to be backed up (check DB names with 'mysql -e "show databases"')
if [ "$1" != "" ]; then db_name=$1; else sleep 0
  #db_name=information_schema
  #db_name=dqsegdb
  #db_name=dqsegdb_regression_tests
  #db_name=mysql
  #db_name=performance_schema
  #db_name=segments_backup
  #db_name=segments_backup_bck_Sep222015
  #db_name=segments_web
  #db_name=dqsegdb_web
  db_name=dqsegdb_web_new
  #db_name=test
fi
# This is the dir that the uncompressed DB will be backed up to
  #raw_dir=/backup/segdb/segments/tmp/mysql_dump
  #raw_dir=/backup/segdb/segments/install_support/tmp/mysql_dump
  #raw_dir=/backup/segdb/segments/install_support/tmp/mysql_dump_segments-web-$(date +%Y.%m.%d-%H.%M.%S)
  #raw_dir=/backup/segdb/reference/install_support/segments-web/segments_web_db/
  raw_dir=/backup/segdb/reference/install_support/segments-web/segments_web_new_db/
#  raw_dir=/tmp/mysql_dump_dqsegdb_web-$(date +%Y.%m.%d-%H.%M.%S)   ### why doesn't this work anymore??
# This is the final dir that the DB will be saved to
  #final_dir=/backup/segdb/reference/install_support/segments-web/dqsegdb_web_db/
  final_dir=/backup/segdb/reference/install_support/segments-web/dqsegdb_web_new_db/
# This determines whether the DB is compressed, and if so, what dir the tarball ends up in
  compress_db=0
  #tarball_dir=/backup/segdb/segments/primary
  #tarball_dir=/backup/segdb/segments/install_support/tmp
  #tarball_dir=/backup/segdb/reference/machine_backups/segments-web_DBs_2019.02.22
  tarball_dir=/backup/segdb/reference/install_support/segments-web/segments_web_new_db/
# Construct tarball name, as $backup_name_part1$backup_namepart2$backup_name_part3 - be sure to add any underscores that you want!; ".tgz" will be added automatically
  backup_name_part1=""
  backup_name_part2=$db_name"_"
  backup_name_part3=""
# Print lots of messages?
  verbose=1

# Set backup date and time
backup_date=$(date +%Y-%m-%d)
backup_name=$backup_name_part1$backup_name_part2$backup_name_part3$backup_date

# Print info about the run
if [ $db_name == "" ]; then echo "### ERROR ### The 'db_name' variable is not set.  Please give it a value and re-run this script."; exit 101; fi
if [ $verbose -eq 1 ]
then
  echo "### INFO ### Backup starting date and time: `date`"
  echo "         ### Database to backup:  $db_name"
  echo "         ### Raw output directory:  $raw_dir"
  if [ $compress_db -eq 1 ]; then echo "         ### Tarball output directory:  $tarball_dir"; echo "         ### Tarball file name: $backup_name.tgz"; fi
fi

# Check for and maybe create raw output dir
if [ $verbose -eq 1 ]; then echo "### INFO ### Checking raw output directory..."; fi
if [ ! -d $raw_dir ]
then
  if [ $verbose -eq 1 ]; then echo "         ### Raw output dir does not yet exist.  Atempting to create it..."; fi
  mkdir -p $raw_dir
  chmod 777 $raw_dir
else
  if [ $verbose -eq 1 ]; then echo "         ### Good - raw output dir already exists."; fi
fi
if [ ! -d $raw_dir ]; then echo "### ERROR ### Could not create raw output dir ( "$raw_dir" ).  Can't fix this.  Exiting."; exit 102; fi

# Check that the raw output dir is empty
raw_dir_contents=`ls -1 $raw_dir | wc -c`
if [ $raw_dir_contents -ne 0 ]
then
  echo "### ERROR ### The raw output dir ( $raw_dir ) is not empty.  Please empty it or pick a new dir.  Exiting."
  exit 103
else
  echo "         ### Good - raw output dir is already empty."
fi

# Check that the raw output dir is writeable by this user (should be root) and user 'mysql'
if [ -w $raw_dir ]
then
  if [ $verbose -eq 1 ]; then echo "         ### Good - raw output dir is writeable by this user (`id -un`)."
else
  echo "### ERROR ### Raw output dir ( "$raw_dir" ) is not writeable by this user (`id -un`).  You should fix this or pick a new dir.  Exiting."; exit 104; fi
fi
#export script_raw_dir=$raw_dir
#if sudo -u mysql bash -c '[[ -w $raw_dir ]]'
#if sudo -u mysql bash -c 'touch '
echo $raw_dir > /tmp/raw_dir.txt
if sudo -u mysql bash -c '[[ -w `cat /tmp/raw_dir.txt` ]]'
then
  if [ $verbose -eq 1 ]; then echo "         ### Good - raw output dir is writeable by user 'mysql'."; fi
else
  echo "### ERROR ### Raw output dir ( "$raw_dir" ) is not writeable by user 'mysql'.  You should fix this or pick a new dir.  Exiting."
  exit 105
fi
rm /tmp/raw_dir.txt

# If appropriate: check for and maybe create tarball output dir; make sure it's writeable; check for existence of final tarball file
if [ $compress_db -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Checking for existence of tarball output directory..."; fi
  if [ ! -d $tarball_dir ]
  then
    if [ $verbose -eq 1 ]; then echo "         ### Tarball output dir does not yet exist.  Atempting to create it..."; fi
    mkdir -p $tarball_dir
  else
    if [ $verbose -eq 1 ]; then echo "         ### Good - tarball output dir already exists."; fi
  fi
  if [ ! -d $tarball_dir ]; then echo "### ERROR ### Could not create raw output dir ( "$tarball_dir" ).  Can't fix this.  Exiting."; exit 106; fi
  if [ -w $tarball_dir ]
  then
    if [ $verbose -eq 1 ]; then echo "         ### Good - tarball output dir is writeable by this user (`id -un`)."
  else
    echo "### ERROR ### Tarball output dir ( "$tarball_dir" ) is not writeable by this user (`id -un`).  You should fix this or pick a new dir.  Exiting."; exit 107; fi
  fi
# we don't need/want the following block, b/c we'll be creating a new backup every night and overwriting the old one
#  if [ $verbose -eq 1 ]; then echo "### INFO ### Checking for existing tarball file..."; fi
#  if [ -e $tarball_dir/$backup_name".tgz" ]
#  then
#    echo "### ERROR ### A file already exists with the backup file name ( "$tarball_dir/$backup_name".tgz )!"
#    echo "          ### Please fix the conflict before re-running this script.  Exiting."
#    exit 108
#  else
#    if [ $verbose -eq 1 ]; then echo "         ### Good - tarball doesn't already exist."; fi
#  fi
fi

# both user root and mysql (?) need to be able to write to the dir;
echo "### INFO ### raw_dir = $raw_dir"
chmod a+rwx $raw_dir

# Do the backup
if [ $verbose -eq 1 ]; then echo "### INFO ### Backing up DB $db_name ..."; fi
time mysqldump --single-transaction -u root $db_name --tab=$raw_dir
output_exit_code=$?

# Compress the file if requested and possible
if [ $compress_db -eq 1 ]
then
  if [ $verbose -eq 1 ]; then echo "### INFO ### Tarballing the backed-up DB..."; fi
  tar zcvf  $tarball_dir/$backup_name".tgz"  -C $raw_dir  .
  if [ -e $tarball_dir/$backup_name".tgz" ]
  then
    if [ $verbose -eq 1 ]; then echo "### INFO ### Tarball successfully created:"; ls -lh $tarball_dir/$backup_name".tgz"; fi
  else
    echo "### WARNING ### Something went wrong with the tarball step.  It doesn't look like the output file ( "$backup_name".tgz ) was created in dir "$tarball_dir
    echo "            ### You might want to do something about that."
  fi
fi

# Move files to backup dir and delete tmp dir
echo "### INFO ### Moving dqsegdb_web files from temp dir to $final_dir"
mv $raw_dir/*  $final_dir/
echo "### INFO ### Deleting temp dir"
rm -rf $raw_dir

echo "### INFO ### Backup complete.  Contents of dir  $final_dir/ :"
ls -l $final_dir/
echo "### INFO ### Backup ending date and time: `date`"
#echo "### NOTICE ### The files from the backed-up DB (in $raw_dir ), and any dir's that were created, are still there."
#echo "           ### You have to decide whether to delete them, and if so, do that yourself."
#echo "           ### (Maybe something like:  #rm -f $raw_dir/* )"

end_time=$(date +%H:%M:%S)
end_sec=$(date -u +"%s")
duration_sec=$((end_sec - start_sec))
echo "### INFO ### Writing run timing info to log file ('$start_date  $start_time - $end_time = $duration_sec sec' >> $time_log_file)"
echo "$start_date  $start_time - $end_time = $duration_sec sec" >> $time_log_file
#echo $(date +%Y-%m-%d) $(date +'%H:%M:%S') >> /backup/segdb/segments/monitor/backup_dqsegdb_web_new.txt

exit $output_exit_code
