#!/bin/bash
# This script is customized to restore (install) the 'dqsegdb_web_new' DB, which contains information for the new segments-web website (probably accessible as https://segments-web.ligo.org/dqsegdb_web_2/).
# Some comments are included because they have information about other DBs, just for reference.
# Run as:   populate_from_backup_dqsegdb_web_new.sh  [backup_dir]  [db_name]
#   where:  [backup_dir] = a dir that already has the unpacked DB files (i.e., the .txt and .sql files) in it; .tar.gz or .tgz isn't enough - they have to be unpacked already
#     and:  [db_name] = the name that the DB will have when it's populated
# to do:
#   * maybe add the option to untarball a file, if that's all that is in the backup dir
#   * add a check that the user is ldbd (if that's still important)
#   * add a message about how to check that the DB restore worked

echo "### INFO ### Starting 'populate_from_backup_dqsegdb_web_new.sh'"
# Set the following line to the directory with the backed-up files in it (e.g., tbl_dq_flags.sql et al.) (this should NOT be a tarball file!)
if [ "$1" != "" ]; then backup_dir=$1; else   ### if there's a command-line arg, that will be the source of the DB files; if not, use the dir specified below
  #backup_dir=/backup/segdb/segments/install_support/tmp/mysql/
  #backup_dir=/backup/segdb/reference/install_support/segments-web/dqsegdb_web_db/
  backup_dir=/backup/segdb/reference/install_support/segments-web/dqsegdb_web_new_db/
fi
# Set the following line to the name of the DB that you want to create and populate (WARNING: Note that if it exists, it will be deleted first!)
if [ "$2" != "" ]; then db_name=$2; else
  #db_name="dqsegdb"   ### this is the name of the DB on segments.ligo.org
  #db_name="segments_backup"   ### this is the name for 'dqsegdb' DB on segments-backup
  #db_name="dqsegdb_regression_tests"   ### this is the name of the regression tests DB on segments-backup
  #db_name="dqsegdb_web"   ### this is the name of the main website DB on segments-web.ligo.org
  db_name="dqsegdb_web_new"   ### this is the name of the new website DB on segments-web.ligo.org
fi
echo "### INFO ### Source for DB files is: " $backup_dir
echo "### INFO ### Database name to be populated is: " $db_name

if [ ! -d $backup_dir ]; then
  echo "### ERROR ### The source for DB files to be restored ( $backup_dir ) does not exist."
  echo "          ### Can't fix this, so exiting."
  exit 1
fi
if [ -e $backup_dir/tbl_contents.sql ]; then
  echo "## INFO ##: Files seem to be available in the backup directory ($backup_dir)."
else
  echo "## ERROR ##: The backup directory ($backup_dir) does not seem to have the expected files, such as tbl_contents.sql."
  echo "             Please put the files there or change the 'backup_dir' variable in the script (probably named 'populate_from_backup_dqsegdb_web_new.sh')."
  exit 2
fi

cd $backup_dir

# To import from --tab option

date1=$(date -u +"%s")
# First clean the database out:
mysql -u root -e "DROP DATABASE IF EXISTS $db_name"
mysql -u root -e "CREATE DATABASE $db_name"
mysql -u root -e "use $db_name"
mysql -u root -e "GRANT SELECT, INSERT, UPDATE ON $db_name.* TO 'dqsegdb_user'@'localhost'"
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'"

# Next create the structure:
# for segments_web, contents are     tbl_{contents,file_metadata,value_groups,values}
# for segments_web_new, contents are tbl_{contents,file_metadata,hosts,output_formats,users}
cat $backup_dir/tbl_contents.sql       | mysql -u root $db_name
cat $backup_dir/tbl_file_metadata.sql  | mysql -u root $db_name
#cat $backup_dir/tbl_value_groups.sql | mysql -u root $db_name
#cat $backup_dir/tbl_values.sql       | mysql -u root $db_name
cat $backup_dir/tbl_hosts.sql          | mysql -u root $db_name
cat $backup_dir/tbl_output_formats.sql | mysql -u root $db_name
cat $backup_dir/tbl_users.sql          | mysql -u root $db_name
#cat $backup_dir/tbl_segments.sql         | mysql -u root $db_name
#cat $backup_dir/tbl_segment_summary.sql  | mysql -u root $db_name

# Then import the data
# for segments_web, contents are     tbl_{contents,file_metadata,value_groups,values}
# for segments_web_new, contents are tbl_{contents,file_metadata,hosts,output_formats,users}
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_contents.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_file_metadata.txt
#mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_value_groups.txt
#mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_values.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_hosts.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_output_formats.txt
mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_users.txt
#mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_segments.txt
#mysqlimport -u root --use-threads=4 --local $db_name $backup_dir/tbl_segment_summary.txt
# Note that doing it this way doubles the import speed by running the two segment tables in parallel

date2=$(date -u +"%s")
diff=$(($date2-$date1))
# Following line commented out for directed DB restore
#echo "$(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import." >> /usr1/ldbd/backup_logging/import_time.log
echo "## INFO ## $(date) : $(($diff / 60)) minutes and $(($diff % 60)) seconds elapsed for import."

echo "## INFO ##: Database restore complete.  If there were no error messages reported, you could probably check that it worked somehow, but I'm not sure how yet."
