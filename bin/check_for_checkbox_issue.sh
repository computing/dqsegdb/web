#!/bin/bash
# A quick script to run the command to check if someone submitted queries to -web but didn't get results b/c they didn't check the 'version' checkbox.

echo "### Running ' grep \"req=retrieve_segments\" /var/log/httpd/ssl_access_log* | grep \"200 -\" '"
echo "###   (200 = successful query; following number = bytes returned; '-' = no bytes returned = nothing happens for the user, including no error)"
grep "req=retrieve_segments" /var/log/httpd/ssl_access_log* | grep "200 -"

echo 
echo "### To invesitage further, set 'name=' and then run these commands:"
echo "### To see if they ever checked the checkbox: ' grep \"\${name}.*checkbox_checked.png\" /var/log/httpd/ssl_access_log* ' "
echo "### (something else): ' grep -e \$name  /var/log/httpd/ssl_access_log*  |  grep -e \$name -e checkbox -e '200 -'  ' "
echo "### Common sequence, with successful resolution (from /var/log/httpd/ssl_access_log-20220828):"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:42:36 -0700] \"GET /images/checkbox.png HTTP/1.1\" 200 142   [checkbox is not checked]"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:43:28 -0700] \"GET /scripts/actions.php?req=retrieve_segments&s=1238253974.34375&e=1248253974.34375&format=json&history=off HTTP/1.1\" 200 -   [failed query - no results]"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:43:39 -0700] \"GET /images/checkbox_checked.png HTTP/1.1\" 200 169   [checkbox is now checked]"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:43:40 -0700] \"GET /scripts/actions.php?req=retrieve_segments&s=1238253974.34375&e=1248253974.34375&format=json&history=off HTTP/1.1\" 200 25   [successful query]"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:43:46 -0700] \"GET /scripts/actions.php?req=get_recent_query_results&home=1 HTTP/1.1\" 200 2983   [successful query]"
echo "[redacted IP address] - [redacted name]@LIGO.ORG [23/Aug/2022:12:43:46 -0700] \"GET /downloads/1661283826.json HTTP/1.1\" 200 8433885   [successful query]"

echo
echo "### Automated checking of email addresses (using 'grep "req=retrieve_segments" /var/log/httpd/ssl_access_log* | grep "200 -" | grep -oE " [[:graph:]]+@" | grep -oE "[[:alpha:]]+\.[[:alpha:]]+" | sort | uniq', then processing),"
echo "###   then 'grep \"\${name}.*checkbox_checked.png\" /var/log/httpd/ssl_access_log* &> /dev/null' and checking exit code (\$?)"
for name in `grep "req=retrieve_segments" /var/log/httpd/ssl_access_log* | grep "200 -" | grep -oE " [[:graph:]]+@" | grep -oE "[[:alpha:]]+\.[[:alpha:]]+" | sort | uniq`
do
  echo "Checking $name"
  grep "${name}.*checkbox_checked.png" /var/log/httpd/ssl_access_log* &> /dev/null
  exit_code=$?
  if [ "$exit_code" -eq 0 ]
  then
    echo "Check - OK"
  else
    echo "### WARNING ### Looks like '$name' never accessed the checked-checkbox file."
  fi
done
